package it.keybiz.javaex;


public class Prodotti {
	public enum Categoria {
		ALIMENTARI, BELLEZZA, CASALINGHI, OFFICINA, INFORMATICA
	}
	
	String titolo;
	Categoria categoria;
	int prezzo;

	public Prodotti(String nome, Categoria categoria, int prezzo) {
		this.titolo = nome;
		this.categoria = categoria;
		this.prezzo = prezzo;
	}

	public String getTitolo() {
		return titolo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public int getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String toString() {
		return titolo + " (" + categoria + ", " + prezzo + "€)";
	}

}