package it.keybiz.javaex;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamProdotti {

    // mi appoggio ad un factory method che genera a rotazione
    // la stessa lista di libri
    public List<Prodotti> generaListaProdotti(int n) {
        return Stream.generate(Magazziniere::gen)
            .limit(n)
            .collect(Collectors.toList());
    }

    public long contaProdottiDiBellezza(List<Prodotti> list) {
        return list.stream().filter(prodotti ->
                prodotti.getCategoria().equals(Prodotti.Categoria.BELLEZZA)).count();
    }

    public List<Prodotti> prezzoCompresoTra12e15(List<Prodotti> list) {
        return list.stream().filter(prodotti ->
                prodotti.getPrezzo() >=12 && prodotti.getPrezzo() <=15).collect(Collectors.toList());
    }

    public List<String> filtraListaProdottiAlimentariEBellezza(List<Prodotti> list) {
        List<Prodotti> list_one = list.stream().filter(
                prodotti -> prodotti.getCategoria().equals(Prodotti.Categoria.BELLEZZA) ||
                        prodotti.getCategoria().equals(Prodotti.Categoria.ALIMENTARI)
        ).collect(Collectors.toList());
        return list_one.stream().map(Prodotti::getTitolo).collect(Collectors.toList());
    }

    public List<Prodotti> generaListaProdottiAlimentari(int n) {
        return Stream.generate(() ->
                new Prodotti("Prodotto alimentare", Prodotti.Categoria.ALIMENTARI, 12))
                .limit(n).collect(Collectors.toList());
    }

    public boolean checkSePresenteBurningChrome(List<Prodotti> list) {
        return list.stream().map(Prodotti::getTitolo).equals("Burning Chrome");
    }

    public int sommaCosti_reduce(List<Prodotti> list) {
        return list.stream().map(Prodotti::getPrezzo).reduce(0, Integer::sum);
    }

    public int sommaCosti_sum(List<Prodotti> list) {
        return list.stream().mapToInt(Prodotti::getPrezzo).sum();
    }

    public double sommaCostiInDollari(double EUR_USD, List<Prodotti> list) {
        double sum = list.stream()
                .mapToInt(Prodotti::getPrezzo).sum();
        return sum * EUR_USD;
    }

    public Optional<Prodotti> prodottoMenoCaroDa12InSu(List<Prodotti> list) {

        List<Prodotti> list_one =  list.stream().filter(prodotti -> prodotti.getPrezzo() >= 12)
                .collect(Collectors.toList());

        return list_one.stream().min(Comparator.comparing(Prodotti::getPrezzo));
    }

    public List<Prodotti> prodottoOrdinatiPerPrezzo(List<Prodotti> list) {
        return list.stream()
                .sorted(Comparator.comparing(Prodotti::getPrezzo)).collect(Collectors.toList());
    }

    // Titolo: "Pozione Harry Potter 1" "Pozione Harry Potter 2"... "Pozione Harry Potter n"
    // categoria: ALIMENTARI, prezzo: 15 euro
    public List<Prodotti> generaPozioneHarryPotterDa15Euro(int n) {
        /*List<Prodotti> list_one = new ArrayList<>();
        for (int i=1; i<= n; i++ ){
            String name = "Harry Potter ";
            name += i;
            list_one.add(new Prodotti( name, Prodotti.Categoria.ALIMENTARI, 15));

        }
        return list_one;*/
        AtomicInteger counter = new AtomicInteger(1);

        return Stream.generate(() -> new Prodotti("Harry Potter " +
                        counter.getAndIncrement(), Prodotti.Categoria.ALIMENTARI, 15))
                .limit(n).collect(Collectors.toList());
    }

    public List<Prodotti> mescolaLista(List<Prodotti> list) {
        Collections.shuffle(list);
        return list;
    }

    public Optional<Prodotti> primoPiuCaroDelPrecedente(List<Prodotti> list) {
        return list.stream()
                .max(Comparator.comparing(Prodotti::getPrezzo));
    }

}
